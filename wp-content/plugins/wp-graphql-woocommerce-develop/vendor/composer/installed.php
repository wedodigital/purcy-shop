<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '86c7fb8b4e57453fe84a9a2f79508b883e8c72d0',
        'name' => 'wp-graphql/wp-graphql-woocommerce',
        'dev' => false,
    ),
    'versions' => array(
        'firebase/php-jwt' => array(
            'pretty_version' => 'v5.3.0',
            'version' => '5.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../firebase/php-jwt',
            'aliases' => array(),
            'reference' => '3c2d70f2e64e2922345e89f2ceae47d2463faae1',
            'dev_requirement' => false,
        ),
        'wp-graphql/wp-graphql-woocommerce' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '86c7fb8b4e57453fe84a9a2f79508b883e8c72d0',
            'dev_requirement' => false,
        ),
    ),
);
