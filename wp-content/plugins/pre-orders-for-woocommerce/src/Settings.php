<?php
namespace Woocommerce_Preorders;

class Settings {
    /**
     * @var mixed
     */
    protected $pluginBase = WCPO_PLUGIN_BASE;

    public function __construct() {
        add_filter( 'woocommerce_settings_tabs_array', [$this, 'addSettingsTab'], 50 );
        add_action( 'woocommerce_settings_tabs_settings_tab_preorders', [$this, 'settingsTab'] );
        add_action( 'woocommerce_update_options_settings_tab_preorders', [$this, 'updateSettings'] );
        add_filter( "plugin_action_links_$this->pluginBase", [$this, 'wcpo_plugin_settings_link'] );
    }

    /**
     * @param $links
     */
    public function wcpo_plugin_settings_link( $links ) {
        $row_meta = array(
            'settings' => '<a href="' . get_admin_url( null, 'admin.php?page=wc-settings&tab=settings_tab_preorders' ) . '">' . __( 'Settings', 'preorders' ) . '</a>',
            'pro_link' => '<a href="' . esc_url( 'https://brightplugins.com/woocommerce-preorder-plugin-review/' ) . '" target="_blank" aria-label="' . esc_attr__( 'Pro Version', 'wpgs' ) . '" style="color:green;font-weight:600;">' . esc_html__( 'Pro Version', 'preorders' ) . '</a>',
        );

        return array_merge( $links, $row_meta );
    }

    /**
     * @param  $settings_tabs
     * @return mixed
     */
    public function addSettingsTab( $settings_tabs ) {
        $settings_tabs['settings_tab_preorders'] = __( 'Preorders', 'preorders' );

        return $settings_tabs;
    }

    public function settingsTab() {
        do_action( 'before_preoder_settings_tab' );
        woocommerce_admin_fields( $this->getSettings() );
    }

    public function updateSettings() {
        woocommerce_update_options( $this->getSettings() );
    }

    public function getSettings() {
        update_option( 'wc_preorders_mode', 'either' );

        $settings = [
            'section_title'             => [
                'name' => __( 'Pre-order Settings', 'preorders' ),
                'type' => 'title',
                'id'   => 'wc_settings_tab_preorders_section_title',
            ],
            'preorders_mode'            => [
                'name'    => __( 'Preorder Modes', 'preorders' ),
                'type'    => 'select',
                'class'   => 'wc-enhanced-select',
                'css'     => 'min-width: 350px;',
                'options' => [
                    'q1'     => __( 'Treat the entire order as preordered - Available in Pro', 'preorders' ),
                    'q2'     => __( 'Generate a separate order with all preordered products - Available in Pro', 'preorders' ),
                    'q3'     => __( 'Generate a separate order for each preordered products - Available in Pro', 'preorders' ),
                    'either' => __( 'Order only preorder products or available ones', 'preorders' ),
                ],
                'id'      => 'wc_preorders_mode',
            ],
            'change_button'             => [
                'name'    => __( 'Change button title for preorder products', 'preorders' ),
                'type'    => 'checkbox',
                'std'     => 'yes',
                'default' => 'yes',
                'id'      => 'wc_preorders_change_button',
            ],
            'change_button_text'        => [
                'name'    => __( 'Add Button Title', 'preorders' ),
                'type'    => 'text',
                'std'     => 'Preorder Now!',
                'default' => 'Preorder Now!',
                'id'      => 'wc_preorders_button_text',
            ],
            'enable_avaiable_date_1'    => [
                'name'    => __( 'Display the Available Date', 'preorders' ),
                'id'      => 'wc_preorders_available_date_single_product',
                'default' => 'yes',
                'type'    => 'checkbox',

                'desc'    => __( 'Single Product Page', 'preorders' ),
            ],
            'change_avaiable_date_text' => [
                'name'    => __( 'Available date Text', 'preorders' ),
                'type'    => 'text',
                'default' => 'Available on {date_format}',
                'desc'    => '
                Choose how the available date should be displayed. <br/>
                <code>{date_format}</code> = Default site date format (Ex: January 15, 2020 )',

                'id'      => 'wc_preorders_avaiable_date_text',
            ],
            'change_cart_notice_text'   => [
                'name'    => __( 'Preorder Cart Notice', 'preorders' ),
                'type'    => 'text',
                'default' => 'Note: this item will be available for shipping in {days_left} days',
                'desc'    => '
                    Text will be shown below the preoder product on the cart page<br/>
                    <strong>Available vars:</strong><br/>
                    <code>{days_left}</code> = Count preorder date',

                'id'      => 'wc_preorders_cart_product_text',
            ],
            'section_end'               => [
                'type' => 'sectionend',
                'id'   => 'wc_settings_tab_preorders_section_end',
            ],
        ];

        return apply_filters( 'wc_settings_tab_preorders_settings', $settings );
    }
}
