<?php
/**
 * Plugin Name: Preorders for WooCommerce
 * Plugin URI: https://brightplugins.com/
 * Description: Ultimate Preorders Plugin for WooCommerce.
 * Version: 1.2.2
 * Domain Path: /etc/i18n/languages/.
 * WC tested up to: 5.5.2
 * Tested up to: 5.8
 * WC requires at least: 3.9
 * Author: Bright Plugins
 * Author URI: https://brightplugins.com
 * Text Domain: preorders
 */

defined( 'ABSPATH' ) || exit;

// Define WCPO_PLUGIN_DIR.
if ( !defined( 'WCPO_PLUGIN_DIR' ) ) {
    define( 'WCPO_PLUGIN_DIR', __DIR__ );
}
if ( !defined( 'WCPO_PLUGIN_BASE' ) ) {
    define( 'WCPO_PLUGIN_BASE', plugin_basename( __FILE__ ) );
}
define( 'WCPO_PLUGIN_VER', '1.2.2' );

use Woocommerce_Preorders\Bootstrap;
use Woocommerce_Preorders\Packages;

require_once WCPO_PLUGIN_DIR . '/vendor/autoload.php';
/**
 * Initialize the plugin tracker
 *
 */
function appsero_init_tracker_pre_orders_for_woocommerce() {

    if ( !class_exists( 'Appsero\Client' ) ) {
        require_once __DIR__ . '/appsero/src/Client.php';
    }

    $client = new Appsero\Client( '4ec1293f-9d9f-4a3d-b312-c93c19e16be8', 'Preorders for WooCommerce', __FILE__ );

    // Active insights
    $client->insights()->init();

}

appsero_init_tracker_pre_orders_for_woocommerce();

// Check if WooCommerce is active
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) ) {
    // Put your plugin code here

    add_action( 'woocommerce_loaded', function () {

        require_once WCPO_PLUGIN_DIR . '/etc/conf.php';

        $bootstrap = new Bootstrap();
        register_activation_hook( __FILE__, [$bootstrap, 'defaultOptions'] );

        $package = new Packages();
    } );
} else {
    add_action( 'admin_notices', function () {
        $class   = 'notice notice-error';
        $message = __( 'Oops! looks like WooCommerce is disabled. Please, enable it in order to use WooCommerce Pre-Orders.', 'preorders' );
        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
    } );
}

if ( get_option( 'bpwcavi-dismiss' ) ) {
    add_action( 'before_preoder_settings_tab', 'bpWaviPlugin' );
}
function bpWaviPlugin() {
    ?>
    <div class="notice notice-success bp-new-alert">
        <div class="bp-logo"><img src="<?php echo WCPO_PLUGIN_URL . 'media/img/bp-logo.png'; ?>" alt="Bright Plugins logo"><span>30% OFF</span></div>
        <div class="exclusive-txt">Exclusive Deal on our new plugin: </div>
        <div class="wcavi-img-banner">
            <img width="100%" src="<?php echo WCPO_PLUGIN_URL . 'media/img/Variation-Images-for-WooCommerce-600x383.png'; ?>" alt="Bright Plugins logo">

        </div>
        <a target="_blank" class="new-link-bp" href="https://brightplugins.com/additional-variation-images-for-woocommerce/" >Additional Variation Images for WooCommerce </a>

    </div>
    <?php
}

add_action( 'before_preoder_settings_tab', 'bvPreorderAdminBox' );

function bvPreorderAdminBox() {
    ?>
    <div class="preorder-proversion-box">
        <h2 class="pp-heading">Preorder Pro Features</h2>
        <ul>
            <li><span class="dashicons dashicons-yes"></span> Unlock 3 more Proerder Modes</li>
            <li><span class="dashicons dashicons-yes"></span> Specific Coupons for Preorder Products</li>
            <li><span class="dashicons dashicons-yes"></span> Customizable Email Notifications for Proerders</li>

            <li><span class="dashicons dashicons-yes"></span> Add Manual Preorder from Order Backend</li>
            <li><span class="dashicons dashicons-yes"></span> Apply Bulk 'Preorder' Action by Category</li>
            <li><span class="dashicons dashicons-yes"></span> Disable Payment Methods for Preoders</li>
            <li><span class="dashicons dashicons-yes"></span> Predefine Product status when preorder period ends</li>
        </ul>
        <a target="_blank" class="button" href="https://brightplugins.com/go/preorder-pro" >Get the Pro Version</a>

        <div class="bp-free-support">
            <span class="dashicons dashicons-sos"></span>
            <a target="_blank" href="https://brightplugins.com/support/" >
                <h4>Ticket Support </h4>
                <p>Direct help from our support team</p>
            </a>

        </div>

        <div class="bp-pro-affiliate">
            <span class="dashicons dashicons-money-alt"></span>
            <a target="_blank" href="https://brightplugins.com/affiliates-terms-and-conditions/" >
                <h4>Affiliate Program </h4>
                <p>Earn flat 30% on every sale!</p>
            </a>

        </div>
    </div>
    <?php
}
